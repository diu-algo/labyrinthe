class Cellule:
    def __init__(self, l, c):
        self.nbLigne = l
        self.nbColonne = c
        self.nord = None
        self.sud = None
        self.ouest = None
        self.est = None
        self.liens = {}

    def lier(self, cell, bidi=True):
        self.liens[cell] = True
        if bidi:
            cell.lier(self, False)
        return self


    def delier(self, cell, bidi=True):
        self.liens.delete(cell)
        if bidi:
            cell.delier(self, False)
        return self

    def liens(self):
        return self.liens.keys()

    def estLiee(self, cell):
        return cell in self.liens.keys()

    def voisins(self):
        list = []
        if self.nord:
            list.append(self.nord)
        if self.sud:
            list.append(self.sud)
        if self.est:
            list.append(self.est)
        if self.ouest:
            list.append(self.ouest)
        return list

    def __str__(self) -> object:
        result = "cell("+str(self.row)+", "+str(self.column)+")\n"
        if self.nord:
            result += "nord = " + self.nord.shortStr() + "\n"
        if self.sud:
            result += "sud = " + self.sud.shortStr() + "\n"
        if self.est:
            result += "est = " + self.est.shortStr() + "\n"
        if self.ouest:
            result += "ouest = " + self.ouest.shortStr() + "\n"
        return result

    def shortStr(self):
        result = "cell("+str(self.row)+", "+str(self.column)+")"
        return result


if __name__ == '__main__':
    cell1 = Cellule(2,4)
    cell2 = Cellule(3,5)
    cell1.nord = cell2
    print(cell1)

