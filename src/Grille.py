import math
import random

from Cellule import Cellule


class Grille:
    def __init__(self, nbLignes, nbColonnes):
        self.nbLignes = nbLignes
        self.nbColonnes = nbColonnes
        self.grille = self.prepare_grid()
        self.configure_cells()

    def prepare_grid(self):
        result = []
        for r in range(self.nbLignes):
            nouvelleLigne = []
            for c in range(self.nbColonnes):
                nouvelleLigne.append( Cellule(r, c) )
            result.append(nouvelleLigne)
        return result

    def configure_cells(self):
        for r in range(self.nbLignes):
            for c in range(self.nbColonnes):
                celluleCourante = self.grille[r][c]
                if r > 0:
                    celluleCourante.nord = self.grille[r - 1][c]
                if r < self.nbLignes - 1:
                    celluleCourante.sud = self.grille[r + 1][c]
                if c > 0:
                    celluleCourante.ouest = self.grille[r][c - 1]
                if c < self.nbColonnes - 1:
                    celluleCourante.est = self.grille[r][c + 1]


    def celluleAleatoire(self):
        r = random.randint(self.nbLignes)
        c = random.randint(self.nbColonnes)
        return self.grille[r][c]

    def taille(self):
        return self.nbLignes * self.nbColonnes

    def chaqueLigne(self):
        return self.grille

    def chaqueCellule(self):
        resultat = []
        for r in range(self.nbLignes):
            for c in range(self.nbColonnes):
                resultat.append(self.grille[r][c])
        return resultat


    def afficher(self):
        resultat = []
        for l in range(self.nbLignes):
            resultat.append( ["#"] * (2*self.nbColonnes + 1) )
            milieu = ["#"] * (2*self.nbColonnes + 1)
            for c in range(self.nbColonnes):
                milieu[2*c + 1] = " "
            resultat.append( milieu )
        resultat.append( ["#"] * (2*self.nbColonnes + 1) )

        for l in range(self.nbLignes):
            for c in range(self.nbColonnes):
                centreL = 2 * l + 1
                centreC = 2 * c + 1
                cell = self.grille[l][c]
                if cell.estLiee(cell.nord):
                    resultat[centreL-1][centreC] = " "
                if cell.estLiee(cell.ouest):
                    resultat[centreL][centreC-1] = " "
                if cell.estLiee(cell.est):
                    resultat[centreL][centreC+1] = " "
                if cell.estLiee(cell.sud):
                    resultat[centreL+1][centreC] = " "

        for l in range(self.nbLignes):
            ligne = ""
            for c in range(self.nbColonnes):
                ligne += resultat[2*l][2*c] + resultat[2*l][2*c+1]
            ligne += resultat[2 * l][2 * (c + 1)]
            print(ligne)
            ligne = ""
            for c in range(self.nbColonnes):
                ligne += resultat[2*l+1][2*c] + resultat[2*l+1][2*c+1]
            ligne += resultat[2*l+1][2 * (c + 1)]
            print(ligne)
        ligne = ""
        for c in range(self.nbColonnes):
            ligne += resultat[2*self.nbLignes][2 * c] + resultat[2*self.nbLignes][2 * c + 1]
        ligne += resultat[2*self.nbLignes][2 * (c+1)]
        print(ligne)
