# Projet labyrinthe

Ce projet contient un squelette de projet sur les labyrinthes.

## Prérequis

- installer python 3 (n'importe quelle version devrait marcher). Ne marche pas avec python2.
- installer git (cf. https://www.atlassian.com/git/tutorials/install-git )


## Pour installer les sources

* Dans un terminal, cloner le dépôt dans un répertoire local:
```bash
git clone git@gitlab.inria.fr:diu-algo/labyrinthe.git
```
Cette action va créer un répertoire nommé labyrinthe contenant les sources de l'exemple.

### C'est quoi git?
En résumé, c'est un logiciel qui permet de gérer les versions d'un projet, une tâche qui s'avère vite indispensable dès qu'un projet prend un peu d'ampleur. 
Pour plus d'informations, se reporter à https://git-scm.com/book/fr/v1/Les-bases-de-Git 

## Pour lancer l'exemple

Aller dans le répertoire src et entrer:
```bash
cd src
python ArbreBinaire.py
```

Vous devriez obtenir un résultat similaire à:
```bash
$ python ArbreBinaire.py
#####################
#                   #
# # ####### # ##### #
# # #       # #     #
# ### # ### # ##### #
# #   # #   # #     #
### # ##### # # ### #
#   # #     # # #   #
### ######### # ### #
#   #         # #   #
##### # ##### # # # #
#     # #     # # # #
# ######### # ##### #
# #         # #     #
####### ######### # #
#       #         # #
### ### ####### ### #
#   #   #       #   #
### # # # # ##### # #
#   # # # # #     # #
#####################
```
